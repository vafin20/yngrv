function getTeachers() {
    $.get("http://localhost:8080/teachers", function (data) {
        let html = '';

        for (let i = 0; i < data.length; i++) {
            html += '<tr>' +
                '<td>' + data[i].id + '</td>' +
                '<td>' + data[i].firstName + '</td>' +
                '<td>' + data[i].lastName + '</td>' +
                '</tr>'
        }

        $('#table_header').after(html);
    })
}
function sendFile() {
    // данные для отправки
    let formData = new FormData();
    console.log(formData);
    // забрал файл из input
    let files = ($('#file'))[0]['files'];
    // добавляю файл в formData
    [].forEach.call(files, function (file, i, files) {
        formData.append("file", file);
    });

    $.ajax({
        type: "POST",
        url: "http://localhost:8080/files",
        headers: {
            "Auth": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIyIiwiY3JlYXRlZEF0IjoiMjAyMS0wNS0yMFQxNzowOTo1Ny4wMzkiLCJyb2xlIjoiVVNFUiIsInN0YXRlIjoiQUNUSVZFIiwiZW1haWwiOiJ2YWZpbjIwQGdtYWlsLmNvbSJ9.paH4D-svz32qx3ui4o_oJouItD1eaTrIurWdl4NSJ6Q"
        },
        data: formData,
        processData: false,
        contentType: false
    })
        .done(function (response) {
            let fileUrl = 'http://localhost:8080/files/' + response;
            $('#photo').append('<img src = "' + fileUrl + '"/>');
        })
        .fail(function () {
            alert('Error')
        });
}

function renderingForProducts(products, container) {
    let innerHTML = '<div class=\"row\">';
    for (let i = 0; i < products.length; i++) {
        innerHTML += '<div class=\"products\">';
        innerHTML += '<h2 id=\"products-name\" class=\"shrift\" style=\"font-size: 20px\">' + products[i]['name'] + '</h2>';
        innerHTML += '<img id=\"products-price\" class=\"shrift\" src=\"dbImageProduct/' + products[i]['image'] + '\"</img>';
        innerHTML += '<p id=\"products-price\" class=\"shrift\">' + 'ЦЕНА: ' + products[i]['price'] + '</p>';
        innerHTML += '<p id=\"products-size\" class=\"shrift\">' + 'РАЗМЕР:' + products[i]['size'] + '</p>';
        innerHTML += '<form action=\"javascript:addToBasket(' + products[i]['id'] + ')\">' + '<button type=\"submit\" class=\"test mybtn\" id=\"btnBuy\">В КОРЗИНУ</button></form>';
    }
    innerHTML += '</div>';
    container.html(innerHTML);
}

function showProducts() {
    $.ajax({
        type: "GET",
        url: "http://localhost:8080/products",
        headers: {
            "Auth": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIyIiwiY3JlYXRlZEF0IjoiMjAyMS0wNS0yMFQxNzowOTo1Ny4wMzkiLCJyb2xlIjoiVVNFUiIsInN0YXRlIjoiQUNUSVZFIiwiZW1haWwiOiJ2YWZpbjIwQGdtYWlsLmNvbSJ9.paH4D-svz32qx3ui4o_oJouItD1eaTrIurWdl4NSJ6Q"
        },
        success: function (response) {
            renderingForProducts(response, $('#products'));
        },
        dataType: "json",
        contentType: "application/json",
        encoding: "UTF-8"
    });
}
function renderingForCart(products, container) {
    let innerHTML = '';
    let sum = 0;
    for (let i = 0; i < products.length; i++) {
        innerHTML += '<div style="padding-bottom: 200px">';
        innerHTML += '<div class="line_block">';
        innerHTML += '<img style="margin-left: 40px" style="size: 200px"  src=\"dbImageProduct/' + products[i]['image'] + '\" height="200" width="160"/> </div>';
        innerHTML += '<div class="line_block">';
        innerHTML += '<p>НАЗВАНИЕ ПРОДУКТА: ' + products[i]['name'] + '</p>';
        innerHTML += '<p>ЦВЕТ: ' + products[i]['color'] + '</p>';
        innerHTML += '<p>РАЗМЕР: ' + products[i]['size'] + '</p>';
        innerHTML += '<p>КОЛИЧЕСТВО: ' + products[i]['quantity'] + '</p>';
        innerHTML += '<p>СУММА БЕЗ НДС: ' + String(products[i]['price'] * products[i]['quantity']) + '</p>';
        innerHTML += '<div class="payment"><button onclick="deleteFromCart(' + products[i]['id'] +')">Удалить из корзины</button></div>';
        innerHTML += '</div></div>';
        sum += products[i]['price'] * products[i]['quantity'];
    }
    container.html(innerHTML);
}

function deleteFromCart(productId) {
    let data = {
        "productId": productId
    };
    $.ajax({
        type: "DELETE",
        url: "http://localhost:8080/basket",
        data: JSON.stringify(data),
        headers: {
            "Auth": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIyIiwiY3JlYXRlZEF0IjoiMjAyMS0wNS0yMFQxNzowOTo1Ny4wMzkiLCJyb2xlIjoiVVNFUiIsInN0YXRlIjoiQUNUSVZFIiwiZW1haWwiOiJ2YWZpbjIwQGdtYWlsLmNvbSJ9.paH4D-svz32qx3ui4o_oJouItD1eaTrIurWdl4NSJ6Q"

        },
        success: function (response) {
            renderingForCart(getBasket(), $('#products'));
        },
        dataType: "json",
        contentType: "application/json",
        encoding: "UTF-8"
    });
}
function getSumForCart(products, container) {
    let innerHTML = '';
    let sum = 0;
    for (let i = 0; i < products.length; i++) {
        sum += products[i]['price'] * products[i]['quantity'];
    }
    sum += 399;
    innerHTML += '\n<p style="margin-left: 50px">Стандартная доставка: 399,00 рублей.</p>';
    innerHTML += '\n<p><h5 style="margin-left: 50px">ИТОГ: ' + sum + ' рублей</h5></p>';
    container.html(innerHTML);
}
function searchProducts() {
    let data;
    if ($('#price') === undefined) {
        data = {
            "price": 1000000,
            "size": $('#size').val()[0]
        };
    } else {
        data = {
            "price": $('#price').val(),
            "size": $('#size').val()[0]
        };
    }
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/products",
        data: JSON.stringify(data),
        headers: {
            "Auth": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIyIiwiY3JlYXRlZEF0IjoiMjAyMS0wNS0yMFQxNzowOTo1Ny4wMzkiLCJyb2xlIjoiVVNFUiIsInN0YXRlIjoiQUNUSVZFIiwiZW1haWwiOiJ2YWZpbjIwQGdtYWlsLmNvbSJ9.paH4D-svz32qx3ui4o_oJouItD1eaTrIurWdl4NSJ6Q"

        },
        success: function (response) {
            renderingForProducts(response, $('#products'));
        },
        dataType: "json",
        contentType: "application/json",
        encoding: "UTF-8"
    });
}

function addToBasket(productId) {
    let data = {
        "productId": String(productId)
    };
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/basket/add",
        data: JSON.stringify(data),
        success: function (response) {
            console.log("added to cart");
        },
        datatype: "json",
        headers: {
            "Auth": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIyIiwiY3JlYXRlZEF0IjoiMjAyMS0wNS0yMFQxNzowOTo1Ny4wMzkiLCJyb2xlIjoiVVNFUiIsInN0YXRlIjoiQUNUSVZFIiwiZW1haWwiOiJ2YWZpbjIwQGdtYWlsLmNvbSJ9.paH4D-svz32qx3ui4o_oJouItD1eaTrIurWdl4NSJ6Q"
        },
        contentType: "application/json",
        encoding: "UTF-8"
    });
}

function getBasket() {
    $.ajax({
        type: "GET",
        url: "http://localhost:8080/basket",
        success: function (response) {
            renderingForCart(response, $('#products'));
            getSumForCart(response, $('#total'));
        },
        datatype: "json",
        headers: {
            "Auth": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIyIiwiY3JlYXRlZEF0IjoiMjAyMS0wNS0yMFQxNzowOTo1Ny4wMzkiLCJyb2xlIjoiVVNFUiIsInN0YXRlIjoiQUNUSVZFIiwiZW1haWwiOiJ2YWZpbjIwQGdtYWlsLmNvbSJ9.paH4D-svz32qx3ui4o_oJouItD1eaTrIurWdl4NSJ6Q"
        },
        contentType: "application/json",
        encoding: "UTF-8"
    });
}