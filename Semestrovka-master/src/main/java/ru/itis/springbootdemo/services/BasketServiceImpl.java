package ru.itis.springbootdemo.services;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.itis.springbootdemo.dto.BasketDto;
import ru.itis.springbootdemo.dto.ProductDto;
import ru.itis.springbootdemo.models.Product;
import ru.itis.springbootdemo.models.User;
import ru.itis.springbootdemo.repositories.ProductRepository;
import ru.itis.springbootdemo.repositories.TokenRepository;
import ru.itis.springbootdemo.repositories.UsersRepository;
import ru.itis.springbootdemo.security.details.UserDetailsImpl;
import ru.itis.springbootdemo.security.token.TokenAuthentication;

import javax.transaction.Transactional;
import java.util.List;
import java.util.function.Supplier;

@Component
public class BasketServiceImpl implements BasketService {

    @Autowired
    private ProductRepository productRepository;

    @SneakyThrows
    @Override
    @Transactional
    public List<BasketDto> getBasket() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<Product> productList = user.getProducts();
        return BasketDto.from(productList);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void addToBasket(Long productId) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Product product = productRepository.findById(productId).orElse(null);
        List<User> userList = product.getUsers();
        userList.add(user);
        product.setUsers(userList);
        productRepository.save(product);
    }

    @Override
    @Transactional
    public void deleteFromBasket(Long productId) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Product product = productRepository.findById(productId).orElse(null);
        List<User> userList = product.getUsers();
        userList.remove(user);
        product.setUsers(userList);
        productRepository.save(product);
    }
}
