package ru.itis.springbootdemo.services;

import ru.itis.springbootdemo.dto.BasketDto;
import ru.itis.springbootdemo.dto.ProductDto;

import java.util.List;

public interface BasketService {
    List<BasketDto> getBasket();
    void addToBasket(Long productId);
    void deleteFromBasket(Long productId);
}
