package ru.itis.springbootdemo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.itis.springbootdemo.dto.BasketDto;
import ru.itis.springbootdemo.dto.ProductDto;
import ru.itis.springbootdemo.dto.ProductIdDto;
import ru.itis.springbootdemo.services.BasketService;

import java.util.List;

@Controller
@CrossOrigin(origins = "http://localhost:3000")
public class BasketController {

    @Autowired
    private BasketService basketService;

    @GetMapping("/basket")
    public ResponseEntity<List<BasketDto>> getBasket() {
        return ResponseEntity.ok(basketService.getBasket());
    }

    @PostMapping("/basket/add")
    public ResponseEntity<String> addToBasket(@RequestBody ProductIdDto productIdDto) {

        basketService.addToBasket(Long.parseLong(productIdDto.getProductId()));
        return ResponseEntity.ok(productIdDto.getProductId());
    }

    @DeleteMapping("/basket")
    public ResponseEntity<String> deleteFromBasket(@RequestBody ProductIdDto productIdDto) {
        basketService.deleteFromBasket(Long.parseLong(productIdDto.getProductId()));
        return ResponseEntity.ok(productIdDto.getProductId());
    }
}
