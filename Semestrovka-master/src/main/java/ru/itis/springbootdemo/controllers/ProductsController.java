package ru.itis.springbootdemo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.springbootdemo.dto.ProductDto;
import ru.itis.springbootdemo.dto.ProductDtoForSearchRequest;
import ru.itis.springbootdemo.services.ProductService;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")

public class ProductsController {

    @Autowired
    private ProductService productService;

    @PostMapping("/products")
    public ResponseEntity<List<ProductDto>> postProduct(@RequestBody ProductDtoForSearchRequest productDto) {
        return ResponseEntity.ok(productService.getProductBySizeAndPrice(productDto.getSize(), Integer.parseInt(productDto.getPrice())));
    }

    @GetMapping("/products")
    public ResponseEntity<List<ProductDto>> getAllProducts() {
        return ResponseEntity.ok(productService.getAllProducts());
    }

}
